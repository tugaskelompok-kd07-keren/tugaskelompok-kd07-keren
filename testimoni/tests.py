from django.test import TestCase

# Create your tests here.
from django.test import TestCase
from django.urls import resolve
from django.test.client import Client
from .views import testimoni
from .models import Curhat

# Create your tests here.
class UnitTestCurhatMe(TestCase):
    #tes url ada apa ga
    def test_url_is_exist(self):
        response = Client().get('/testimoni/')
        self.assertEqual(response.status_code, 200)

    #tes url testimoni manggil fungsi apa
    def test_using_index_func(self):
        found = resolve('/testimoni/')
        self.assertEqual(found.func, testimoni)

    #tes url testimoni manggil html apa
    def test_using_to_do_list_template(self):
        response = Client().get('/testimoni/')
        self.assertTemplateUsed(response, 'testimoni.html')

    # cek bisa bikin model apa ga, trus tes jumlahnya
    def test_model_can_create_new_testimoni(self):
        new_testimoni = Curhat.objects.create(testimoni='Aku capek')

        counting_all_available_testimoni = Curhat.objects.all().count()
        self.assertEqual(counting_all_available_testimoni, 1)

    #tes model bisa bikin model apa ga
    def test_model_check_create_new_testimoni(self):
        testimoni = {'testimoni':"aku capek"}
        response = Client().post("/testimoni/", testimoni)
        self.assertEqual(response.status_code, 302)

    # tes url yang gaada
    def test_urls_does_not_exist(self):
        response = Client().get('/aaaaaaa/')
        self.assertEqual(response.status_code,404)

    #gatau sih
    def test_contain_name (self):
        response = Client().get('/testimoni/')
        html = response.content.decode('utf8')
        self.assertIn("testimoni",html)