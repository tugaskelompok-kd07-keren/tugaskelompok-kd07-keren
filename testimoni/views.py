from django.shortcuts import render, redirect
from .models import Curhat
from django.core.exceptions import ValidationError
# from .forms import CurhatForm

# Create your views here.
def testimoni(request):
    context = {}
    if request.method == "POST":
        nama = request.POST.get('nama')
        testi = request.POST.get('testi')
        curhat = Curhat(nama=nama, testi=testi)
        try:
            curhat.clean()
            curhat.save()
        except ValidationError:
            pass
    context['curhatan'] = Curhat.objects.all() 
    return render(request, "testimoni.html", context)