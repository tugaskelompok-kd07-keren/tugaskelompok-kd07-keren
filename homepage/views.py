from django.shortcuts import render, redirect
from .models import Reply
from .import forms

# Create your views here.
def homepage(request):
    return render(request, 'homepage.html')

def postreply(request):
    postreply = Reply.objects.all()
    return render(request, 'postreply.html', {'postreply':postreply})

def reply(request):
    if request.method == 'POST':
        form = forms.ReplyForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('homepage:postreply')

    else:
        form = forms.ReplyForm()
    return render(request, 'reply.html', {'form': form})