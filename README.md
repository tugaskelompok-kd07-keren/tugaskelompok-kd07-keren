Muhammad Akbar Rafsanjhani  - 1806186686

Muhammad Aditya             - 1806147054

Nadia Victoria Aritonang    - 1806235901

Rania Azzahra               - 1806185475

http://curhatme.herokuapp.com



untuk tugas kali ini, kami akan membuat sebuah website curhat, namanya curhatme.
didalam website tersebut, pengguna bisa post curhatan secara anonim.
curhatme merupakan platform curhat yang bisa digunakan oleh siapapun.
pengguna cukup membuka website dan klik tombol curhat. di website curhatme,
pengguna bisa berperan sebagai pendengar ataupun pencurhat, keduanya
bisa anonim, sehingga tidak perlu takut identitas terlihat.
guna nya adalah untuk membantu orang-orang yang memiliki masalha baik masalah
sederhana, pribadi, hingga kompleks. semua bisa dikelompokkan cukup dengan
membuat tags, misalnya ketika curhat tentang demot kuliah, bisa mengisi kolom
tags dengan #demot. tags ini untuk mempermudah pencarian curhat tentang demot
yang juga telah dilengkapi dengan reply mengenai permasalahan tersebut.

fitur-fiturnya:

- kolom posting untuk user curhat

- fitur reply untuk user pendengar

- mengandung auto reply jika mengandung kata kata tertentu

- pembagian tag curhat sesuai dengan masalah yang sedang dialami
 
- kolom search untuk melihat tags curhat apa saja yang ada

status pipelines:
[![pipeline status](https://gitlab.com/tugaskelompok-kd07-keren/tugaskelompok-kd07-keren/badges/master/pipeline.svg)](https://gitlab.com/tugaskelompok-kd07-keren/tugaskelompok-kd07-keren/commits/master)